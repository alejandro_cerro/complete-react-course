console.log("Indecision App is running");
import React from 'react';
import ReactDOM from 'react-dom';
import IndecisionApp from './components/Indecision';
import './styles/styles.scss';
import 'normalize.css/normalize.css';

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
