// Arrow functions
console.log("Arrow Functions");

const square = function (x) {
  return x * x;
}

const squareArrow = (x) => {
  return x * x;
}

console.log(square(10));
console.log(squareArrow(4));

// Arrow function expression syntax

const thirdArrow = (x) => x * x;

console.log("Third Arrow", thirdArrow(5));


// Challenge
// getFirstName('minke smith') -> "mike"
// Create reg arrow func
// Create short syntax func

let fullName = "Mike Func"

const firstName = (fullName) => {
  return fullName.split(' ')[0]; 
}

const shortFunc = (fullName) => fullName.split(' ')[0];

console.log("First Name: ", firstName(fullName));
console.log("Short Function: ", shortFunc('Bill Rogan'));