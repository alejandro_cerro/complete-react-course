console.log("Let's make it visible!!");
// Component Based system

// VisibilityToggle - render, constructor, handleToggleVisibility
class VisibilityToggel extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
    this.state = {
      visibility: false
    }
  }
  handleToggleVisibility() {
    this.setState((prevState) => {
      return {
        visibility: !prevState.visibility
      }
    })
  }
  render() {
    return (
      <div>
        <h1>Visibility Toggle</h1>
        <button onClick={this.handleToggleVisibility}>
          {this.state.visibility ? 'Hide Details' : 'Show Details' }
        </button>
        {this.state.visibility && (
          <div>
            <p>These are some details you should be able to see</p>
          </div>
        )}
      </div>
    );
  }
}

ReactDOM.render(<VisibilityToggel />, document.getElementById('app'));

// visibility -> false

// Initial app
// My solution
// const app = {
//   title: "Visibility Toggle",
//   show: false,
//   toggleText: [
//     "Hide Details",
//     "Show Details"
//   ],
//   options: []
// }

// let markup = '';

// const showHide = () => {
//   if (!app.show) {
//     app.show = true;
//     markup = <p>This is some text that is visible</p>;
//     render();
//   } else {
//     app.show = false;
//     markup = '';
//     render();
//   }
// }

// const appRoot = document.getElementById('app');

// // Template
// const render = () => {
//   const Template = (
//     <div>
//     <h1>{app.title}</h1>
//     <button onClick={showHide}>{app.show ? app.toggleText[0] : app.toggleText[1]}</button>
//     {markup}
//   </div>
//   );
//   ReactDOM.render(Template, appRoot);
// }

// render();

// Video solution
// let visibility = false;
// const toggleVisibility = () => {
//   visibility = !visibility;
//   reRender();
// }

// const reRender = () => {
//   const jsx = (
//     <div>
//       <h1>Visibility Toggle</h1>
//       <button onClick={toggleVisibility}>
//         {visibility ? 'Hide Details' : 'Show Details' }
//       </button>
//       {visibility && (
//         <div>
//           <p>These are some details you should be able to see</p>
//         </div>
//       )}
//     </div>
//   );
//   ReactDOM.render(jsx, document.getElementById('app'));
// }
// reRender();