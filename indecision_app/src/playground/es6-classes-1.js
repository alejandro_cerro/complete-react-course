console.log("Making Classes");

class Person {
  // Constructor
  constructor(name = 'Anonymous', age = 0) {
    this.name = name;
    this.age = age;
  }
  getGreeting() {
    return `Hi, I am ${this.name}`;
  }
  getDescription() {
    return `${this.name} is ${this.age} ${this.age > 1 ? 'years' : 'year' } old.`
  }
}

class Student extends Person {
  constructor(name, age, major) {
    super(name, age);
    this.major = major;
  }
  hasMajor() {
    return !!this.major;
  }
  getDescription() {
    let description = super.getDescription();
    if (this.hasMajor()) {
      description += ` Their major is ${this.major}.`;
    }
    return description;
  }
}

class Traveler extends Person {
  constructor(name, age, location) {
    super(name, age);
    this.homeLocation = location;
  }
  hasLocation() {
    return !!homeLocation();
  }
  getGreeting() {
    let greeting = super.getGreeting();
    if (this.homeLocation) {
      greeting += ` I am visiting from ${this.homeLocation}.`;
    }
    return greeting;
  }
}

const me = new Traveler('Alejandro Cerro', 44, 'Kansas City');
const other = new Traveler();

// console.log(me.hasMajor());
// console.log(other.hasMajor());

console.log("Me: ", me);
console.log("Other: ", other);
console.log(me.getGreeting());
// console.log(me.getDescription());
console.log(other.getGreeting());
// console.log(other.getDescription());