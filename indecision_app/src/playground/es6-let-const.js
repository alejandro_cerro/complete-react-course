console.log("Playground");

var nameVar = "Andrew";
var nameVar = "Shit head"
console.log('nameVar: ', nameVar);


let nameLet = "Jen";
nameLet = "Juliet";
console.log('nameLet: ', nameLet);

const nameConst = 'Frankenstine';
console.log('nameConst: ', nameConst);

function getPetName() {
  let petName = "hal";
  return petName;
}

// block scoping
var fullName = "Alejandro Cerro";
let firstName;
if (fullName) {
  firstName = fullName.split(' ')[0];
  console.log('firstName: ', firstName);
}

console.log('First name from outside: ', firstName);