// Arguments Object
const add = (a, b) => {
  // No argument available
  // console.log("Arguments", arguments);
  return a + b;
}

console.log("Add: ", add(5, 10));

// This

const user = {
  name: 'Alejandro',
  cities: ['KCMO', 'Santa Fe', 'Tulsa'],
  printPlacesLived: function () {
    // Arrow functions bind to their parent scope
    this.cities.forEach((city) => {
      console.log(this.name + ' has lived in ' + city);
    })

    // Returns but doesn't modify the original array
    const cityMessages = this.cities.map((city) => {
      return city + '!';
    })
  },
  // New syntax
  // We can create this as a function directly
  secondPrinter() {
    // Arrow functions bind to their parent scope
    this.cities.forEach((city) => {
      console.log(this.name + ' has lived in ' + city);
    })
  }
}

user.printPlacesLived();
console.log("Separator");
user.secondPrinter();

// Challenge
const multiplier = {
  // array of numbers
  numbers: ['1', '2', '3'],
  // multiplyBy = single number
  multiplyBy: 2,
  // multiply returns new array where number have been multiplied.
  multiply() {
    return this.numbers.map((numbers) => numbers * this.multiplyBy);
  }
}

console.log(multiplier.multiply());