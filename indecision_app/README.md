# Indecision App with React

## To run the program locally without webpack
```
babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch
```

In another console, in the indecision_app folder run
```
live-server public
```