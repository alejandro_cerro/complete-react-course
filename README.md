# Complete React Course
This repo is for keeping track of my progress on the [Complete React Course](https://completereactcourse.com) by [Andrew Mead](https://mead.io). The course walks us through creating three applications using [React](https://reactjs.org) and [Redux](https://redux.js.org) with the additional goal of creating a React boilerplate that can be re-used to quickly spin up an application.

Note: I had to redo this repo, so there are probably issues and no branches.

## Boilerplate

| Version   | Branch    | Tags    |
| ----------|-----------|---------|
| `1`       | N/A       | N/A     |

The boilerplate code.

## Expensify App
| Version   | Branch    | Tags    |
| ----------|-----------|---------|
| `1`       | N/A       | N/A     |

A simple money management app

## Indecision App
| Version   | Branch    | Tags    |
| ----------|-----------|---------|
| `1`       | N/A       | N/A     |

Returns a random "decision" from a list. Sort of like a magic 8 ball

## Portfolio Site
| Version   | Branch    | Tags    |
| ----------|-----------|---------|
| `1`       | N/A       | N/A     |

Portfolio site written in React
