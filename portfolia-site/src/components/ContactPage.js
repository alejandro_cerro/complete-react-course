import React from 'react';

const ContactPage = () => (
  <div>
    <h2>Contact Page</h2>
    <p>You can reach me at email@email.com</p>
  </div>
);

export default ContactPage;