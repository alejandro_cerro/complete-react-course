import React from 'react';

const PortfolioItemPage = (props) => (
  <div>
    <h2>Single Portfolio Page</h2>
    <p>This is page number {props.match.params.id}</p>
  </div>
)

export default PortfolioItemPage;